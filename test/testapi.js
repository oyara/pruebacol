var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')

var should = chai.should()

chai.use(chaiHttp) // Configurar Chai con el módulo http


describe('Pruebas Colombia', () => {
  it('BBVA funciona', (done) => {
    chai.request('http://www.bbva.com')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  });

  it('FIFA funciona', (done) => {
    chai.request('http://www.fifa.com')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  });

  it('Mi API funciona', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end((err, res) => {
          res.should.have.status(200)
          done()
        })
  });

  it('Devuelve un array de usuarios', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          //console.log(res.body)
          res.body.should.be.a('array')
          done()
        })
  });

  it('Devuelve al menos un elemento', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          //console.log(res.body)
          res.body.length.should.be.gte(1)
          done()
        })
  });

  it('Validar primer elemento', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          //console.log(res.body[0])
          res.body[0].should.have.property('nombre')
          res.body[0].should.have.property('apellidos')
          done()
        })
  });

});
/*
describe('Tests de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.es')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          done()
        })
  })
})

describe('Tests de API usuarios', () => {
  it('Raíz OK', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end((err, res) => {
          //console.log(res)
          res.should.have.status(200)
          res.body.mensaje.should.be.eql("Bienvenido a mi API")
          done()
        })
  })
  it('Lista de usuarios', (done) => {
    chai.request('http://localhost:3000')
        .get('/apitechu/v1/usuarios')
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('array')
          for (var i = 0; i < res.body.length; i++) {
            res.body[i].should.have.property('email')
            res.body[i].should.have.property('password')
          }
          done()
        })
  })
})
*/
